//#include <epuck.h>
//#include <controlFunctions.h>
#include "RobotDriver.hpp"

/*****************************/
/**** Main Program ***********/
/*****************************/

void IncorrectArguments(const int& argc);

int main(int argc, char** argv) {
    if (argc != 2) {
        IncorrectArguments(argc);
    }
    std::string nameOfReadFolder = argv[1];
    int cnt = 1;
    Pose curPoseFromEnc, curPoseFromVis, initPose;
    std::vector<double> xEnc, yEnc, thEnc;
    xEnc = loadSensorLogs(nameOfReadFolder + "data/xEnc.txt");
    yEnc = loadSensorLogs(nameOfReadFolder + "data/yEnc.txt");
    thEnc = loadSensorLogs(nameOfReadFolder + "data/thEnc.txt");

    std::vector<double> iRed[8];
    iRed[0] = loadSensorLogs(nameOfReadFolder + "data/ir0.txt");
    iRed[1] = loadSensorLogs(nameOfReadFolder + "data/ir1.txt");
    iRed[2] = loadSensorLogs(nameOfReadFolder + "data/ir2.txt");
    iRed[3] = loadSensorLogs(nameOfReadFolder + "data/ir3.txt");
    iRed[4] = loadSensorLogs(nameOfReadFolder + "data/ir4.txt");
    iRed[5] = loadSensorLogs(nameOfReadFolder + "data/ir5.txt");
    iRed[6] = loadSensorLogs(nameOfReadFolder + "data/ir6.txt");
    iRed[7] = loadSensorLogs(nameOfReadFolder + "data/ir7.txt");
    //std::string nameOfWriteFolder = createLogFolder();
    Logger logger;
    cv::Mat colImgFromDisk;
    initPose.setPose(.32, 0., M_PI);
    RobotParameters rp;
    while(true){
        std::cout << COLOR_COUT_BLUE_BRIGHT;//write in bold cyan
        std::cout << "\nSTART ITERATION " << cnt <<" \n";
        std::cout << COLOR_COUT_RESET;//reset color

        if(cnt == 1) {
            curPoseFromEnc = initPose;
            curPoseFromVis = initPose;
            colImgFromDisk = loadAndShowImageFromDisk(nameOfReadFolder, 2);
        } else {
            curPoseFromEnc.x = xEnc[cnt-1];
            curPoseFromEnc.y = yEnc[cnt-1];
            curPoseFromEnc.th = thEnc[cnt-1];
            colImgFromDisk = loadAndShowImageFromDisk(nameOfReadFolder, cnt); 
        }
        float areaPix;
        cv::Point baryc = processImageToGetBarycenter(colImgFromDisk, areaPix);
        curPoseFromVis = getCurrPoseFromVision(baryc, curPoseFromEnc.th, areaPix, logger);

        std::vector<cv::Point2f> ProxInWFrame;
        float mWorld, pWorld, mRob, pRob;
        bool lineFound;
        std::array<double,8> curIRed;
        for(int i = 0; i <8; i++)
            curIRed[i] = iRed[i][cnt-1];
        convertIRPointsForWallFollowing(rp, curIRed, curPoseFromEnc, ProxInWFrame, mRob, pRob, lineFound, mWorld, pWorld);
        drawMapWithRobot(rp, curPoseFromEnc, curPoseFromVis, ProxInWFrame, lineFound, mWorld, pWorld);

        cv::waitKey(0);
        cnt++;
    }
    return 0;
}


void IncorrectArguments(const int& argc) {
    printf("There are %d arguments instead of 1\n", argc - 1);
    printf("The argument should be the path to the folder containing the images to be processed \n");
    exit(0);
}
