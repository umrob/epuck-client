# Installation guide

### Prerequisites :

* Only for simulations: [CoppeliaSim](https://www.coppeliarobotics.com/downloads)  (EDU version) for linux.
```sh
$ # extract file
$ tar -xf CoppeliaSim_Edu_V4_0_0_Ubuntu18_04.tar.xz
$ # place it in folder home/
$ mv CoppeliaSim_Edu_V4_0_0_Ubuntu18_04 ~
$ # launch the program
$ ~/CoppeliaSim_Edu_V4_0_0_Ubuntu18_04/coppeliaSim.sh
```

* The OpenCV library
```sh
$ sudo apt install libopencv-dev
```

* Softwares CMake and Git
```sh
$ sudo apt install cmake git
```


### Installation and Execution

Download the folder directly or clone it using git

Only for simulations, launch CoppeliaSim and open scene `e-puck.ttt` contained in the epuck-client folder 

Move in folder epuck-client/build/ and run:
```sh
$ cmake ..
$ make
```
To launch the application run:
```sh
$ # lancer la simulation
$ ./apps/epuck-app setWheelCmd 127.0.0.1 5 -5 
$ # pour un robot
$ ./apps/epuck-app setWheelCmd 192.168.1.8 50 -50  
```

This application makes the e-puck move and record data.
A folder is created to save the robot data:

![](logs_tree.png)

To stop the program, click ctrl + c in the terminal.

Ressources:

E-puck version 1 :    
* https://www.gctronic.com/doc/index.php/E-Puck
* https://gitlab.com/umrob/epuck-client

E-puck version 2 :
* https://www.gctronic.com/doc/index.php/e-puck2
* https://www.gctronic.com/doc/index.php?title=e-puck2_PC_side_development
* https://github.com/gctronic/epuck_driver_cpp/blob/e-puck2_wifi/src/epuck2_driver_cpp.cpp

E-puck CoppeliaSim (Vrep) :
* https://www.coppeliarobotics.com/helpFiles/en/remoteApiClientSide.htm
* https://www.coppeliarobotics.com/helpFiles/en/remoteApiFunctions.htm
